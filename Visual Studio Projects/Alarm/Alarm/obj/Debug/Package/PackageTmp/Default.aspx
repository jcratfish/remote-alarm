﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Alarm._Default" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
        <asp:Literal ID="SiteTitle1" runat="server"></asp:Literal></title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/css/jumbotron-narrow.css" rel="stylesheet">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<form id="Form" runat="server">
    <div class="container">
        <div class="header">
            <h3 class="text-muted"><asp:Literal ID="SiteTitle2" runat="server"></asp:Literal></h3>
        </div>
        <div id="ErrorMessageContainer" class="alert alert-danger" role="alert" runat="server">
            <strong>Exception: </strong>
            <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
        </div>
        <div class="jumbotron">
            <h1><asp:Literal ID="SiteMessage" runat="server"></asp:Literal></h1>
            <br />
            <div class="row">
                <div class="col-md-12 form-group">
                    <asp:DropDownList ID="AlarmList" class="form-control" runat="server">
                        <asp:ListItem Enabled="true" Selected="True" Text="Choose..." Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-6 form-group">
                    <asp:Button ID="StartAlarm" runat="server" Text="Start" OnClick="StartAlarm_Click" class="btn btn-danger btn-lg" />
                </div>
                <div class="col-md-6 form-group">
                    <asp:Button ID="StopAlarm" runat="server" Text="Stop" OnClick="StopAlarm_Click" class="btn btn-default btn-lg" />
                </div>
                <div class="col-md-8 col-md-offset-2 form-group" style="margin-bottom:0;">
                    <span class="help-block text-left" style="margin:0;">
                        Clicking the "Start" button will do the following:<br />
                        • Unmute the windows audio device<br />
                        • Set volume level to the currently selected value<br />
                        • Play the selected file or a random file if none is selected
                    </span>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Volume Control<asp:Literal ID="VolumeMuteStatus" runat="server"></asp:Literal></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-inline text-center">
                        <div class="form-group">
                            <asp:LinkButton ID="VolumeMute" class="btn btn-primary" runat="server" onclick="VolumeMute_Click">
                                <span id="VolumeMuteGlyphicon" runat="server"></span>&nbsp;<span id="VolumeMuteText" runat="server"></span>
                            </asp:LinkButton>
                        </div>
                        <div class="form-group">
                            <div id="VolumeSlider" style="width:260px; margin:15px;"></div>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="VolumeLevel" class="form-control text-center" style="width:50px;margin:0 20px;" runat="server"></asp:TextBox>
                        </div>
                        <asp:Button ID="SetVolumeLevel" runat="server" Text="Set Volume Level" class="btn btn-primary" onclick="SetVolumeLevel_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <p>&copy; 2014 | <a href="http://www.jcratfish.com/" target="_blank">Jeremy Ratliff</a></p>
        </div>
    </div>
    <!-- /container -->
</form>
    <!-- Core Javascript Files ========================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- jQuery UI -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>

    <script>
        $(function () {
            $("#VolumeSlider").slider({
                orientation: "horizontal",
                range: "min",
                min: 0,
                max: 100,
                value: <%= Volume %>,
                slide: function (event, ui) {
                    $("#VolumeLevel").val(ui.value);
                }
            });
            $("#VolumeLevel").val($("#VolumeSlider").slider("value"));
        });
	</script>
</body>
</html>