﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Linq;
using System.Diagnostics;
using CoreAudioApi;
using System.Web.UI.HtmlControls;

namespace Alarm
{
    public partial class _Default : System.Web.UI.Page
    {
        // Parameters
        string Version = "1.9.6";
        string SiteTitle;
        string PathToVLC = @"C:\Program Files (x86)\VideoLAN\VLC\vlc.exe";
        string PathToAlarms = @"F:\Websites\alarm\alarms\";
        const string VLCProcessesConst = "VLCProcesses";

        // Lists
        List<string> Files = new List<string>(Directory.EnumerateFiles(@"F:\Websites\alarm\alarms"));
        List<string> MuteMessages = new List<string>(new string[] { "<span class=\"label label-success pull-right\">Not Muted</span>", "<span class=\"label label-danger pull-right\">Muted</span>" });
        List<string> MuteButtons = new List<string>(new string[] { "Unmute", "Mute" });

        // Audio Device
        private MMDevice Device = new MMDeviceEnumerator().GetDefaultAudioEndpoint(EDataFlow.eRender, ERole.eMultimedia);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set site title
                SiteTitle = "Alarm v" + Version;
                SiteTitle1.Text = SiteTitle;
                SiteTitle2.Text = SiteTitle;

                // Set site message
                SiteMessage.Text = "Wake up Jeremy!";

                // Disable elements
                ErrorMessageContainer.Visible = false;
                StopAlarm.Enabled = false;

                // Set mute status
                if (Mute)
                {
                    VolumeMuteStatus.Text = MuteMessages[1];
                    VolumeMuteGlyphicon.Attributes.Add("class", "glyphicon glyphicon-volume-up");
                    VolumeMuteText.InnerText = MuteButtons[0];
                }
                else
                {
                    VolumeMuteStatus.Text = MuteMessages[0];
                    VolumeMuteGlyphicon.Attributes.Add("class", "glyphicon glyphicon-volume-off");
                    VolumeMuteText.InnerText = MuteButtons[1];
                }

                // Add Files to drop down list
                try
                {
                    foreach (var file in Files)
                    {
                        AlarmList.Items.Add(file.Substring(file.LastIndexOf("\\") + 1));
                    }
                }
                catch (Exception ex)
                {
                    ThrowException(ex);
                }
            }
            else
            {
                // Enable Stop Button
                StopAlarm.Enabled = true;
            }
            
        }

        protected void StartAlarm_Click(object sender, EventArgs e)
        {
            try
            {
                // Step 1: Unmute
                    Device.AudioEndpointVolume.Mute = false;

                // Step 2: Set Volume to Selected Level
                    // Convert Volume Level
                    int Volume = Convert.ToInt32(VolumeLevel.Text.ToString());

                    // Set Volume Level
                    Device.AudioEndpointVolume.MasterVolumeLevelScalar = (float)Volume / 100;

                // Step 3: Play the Selected File
                    // Create a new process
                    Process p = new Process();

                    // Set process to open VLC
                    p.StartInfo.FileName = PathToVLC;

                    // Value parameter
                    string selectedValue;

                    // If an audio file is selected, play the selected file. Otherwise, play a random file.
                    if (!string.IsNullOrEmpty(AlarmList.SelectedValue.ToString()))
                    {
                        selectedValue = AlarmList.SelectedValue.ToString();
                    }
                    else
                    {
                        Random rnd = new Random();
                        int r = rnd.Next(Files.Count);
                        selectedValue = Files[r].Substring(Files[r].LastIndexOf("\\") + 1);
                    }

                    // Set the selected audio file
                    p.StartInfo.Arguments = "\"" + PathToAlarms + selectedValue + "\"";

                    // Start the process
                    p.Start();

                    // Add process ID to list
                    VLCProcesses.Add(p.Id);

                    // Let the user know the alarm has been activated
                    SiteMessage.Text = "Alarm Activated!";
                }
                catch (Exception ex)
                {
                    ThrowException(ex);
                }
        }

        protected void StopAlarm_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var VLCProcess in VLCProcesses)
                {
                    Process p = Process.GetProcessById(VLCProcess);
                    p.Kill();
                }

                SiteMessage.Text = "Alarm Silenced!";
            }
            catch (Exception ex)
            {
                ThrowException(ex);
            }
        }

        protected void SetVolumeLevel_Click(object sender, EventArgs e)
        {
            // Convert Volume Level
            int Volume = Convert.ToInt32(VolumeLevel.Text.ToString());
            
            // Set Volume Level
            Device.AudioEndpointVolume.MasterVolumeLevelScalar = (float)Volume / 100;
        }

        protected void VolumeMute_Click(object sender, EventArgs e)
        {
            if (Mute)
            {
                // Unmute
                Device.AudioEndpointVolume.Mute = false;

                // Update text and button
                VolumeMuteStatus.Text = MuteMessages[0];
                VolumeMuteGlyphicon.Attributes.Add("class", "glyphicon glyphicon-volume-off");
                VolumeMuteText.InnerText = MuteButtons[1];
            }
            else
            {
                // Mute
                Device.AudioEndpointVolume.Mute = true;

                // Update text and button
                VolumeMuteStatus.Text = MuteMessages[1];
                VolumeMuteGlyphicon.Attributes.Add("class", "glyphicon glyphicon-volume-up");
                VolumeMuteText.InnerText = MuteButtons[0];
            }
        }

        // Get Volume Level
        public float Volume
        {
            get
            {
                return Device.AudioEndpointVolume.MasterVolumeLevelScalar * 100;
            }
        }

        // Get Mute Status
        public bool Mute
        {
            get
            {
                return Device.AudioEndpointVolume.Mute;
            }
        }

        // VLC Process IDs
        public List<int> VLCProcesses
        {
            get
            {
                if (!(ViewState[VLCProcessesConst] is List<int>))
                {
                    // need to fix the memory and added to viewstate
                    ViewState[VLCProcessesConst] = new List<int>();
                }

                return (List<int>)ViewState[VLCProcessesConst];
            }
        }

        protected void ThrowException(Exception ex)
        {
            ErrorMessageContainer.Visible = true;
            ErrorMessage.Text = ex.ToString();
        }
    }
}